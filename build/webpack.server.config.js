const path = require('path')
const merge = require('webpack-merge')
const dev = require('./webpack.dev.config')

module.exports = merge(dev, {
  devServer: {
    contentBase: path.join(__dirname, '../src/dist'),
    port: 8080,
    open: true,
    overlay: {
      warnings: true,
      errors: true
    }
  }
})
