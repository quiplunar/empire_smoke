const path = require('path')
const glob = require('glob')
const autoprefixer = require('autoprefixer')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: ['./src/static/typescript/main.ts', './src/static/styles/styles.scss'],
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'js/[hash].js'
  },
  module: {
    rules: [
      {
        test: /\.(js|ts)$/,
        use: ['babel-loader']
      },
      {
        test: /\.(scss|sass)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer()
              ]
            }
          },
          'sass-loader'
        ]
      },
      {
        test: /\.(hbs)$/,
        use: [
          {
            loader: 'handlebars-loader'
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js'],
    plugins: [
      new TsconfigPathsPlugin({ configFile: 'tsconfig.json' })
    ]
  },
  devtool: 'source-map',
  target: 'web',
  optimization: {
    noEmitOnErrors: true
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/[hash].css'
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: './src/assets/', to: './assets/' },
        { from: './src/static/misc/', to: '' }
      ]
    }),
    new CleanWebpackPlugin(),
    ...glob.sync('./src/pages/*.hbs').map(htmlFile => {
      return new HtmlWebpackPlugin({
        inject: true,
        interpolate: true,
        filename: path.basename(htmlFile.replace(/\.hbs/, '.html')),
        template: htmlFile
      })
    })
  ]
}
