const merge = require('webpack-merge')
const dev = require('./webpack.dev.config.js')
const ImageminPlugin = require('imagemin-webpack')

module.exports = merge(dev, {
  devtool: 'none',
  plugins: [
    new ImageminPlugin({
      name: '[path][name].[ext]',
      imageminOptions: {
        plugins: [
          [
            'mozjpeg',
            {
              quality: 73,
              progressive: true
            }
          ],
          [
            'pngquant',
            {
              quality: [0.6, 0.8]
            }
          ]
        ]
      }
    })
  ]
})
